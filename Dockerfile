FROM python:3.7

# install build utilities
RUN apt-get update && \
    apt-get install -y --no-install-recommends apt-utils && \
    apt-get install -y libgl1-mesa-glx && \
    apt-get -y upgrade

# clone the repository 
RUN git clone --depth 1 https://github.com/tensorflow/models.git

# Install object detection api dependencies
RUN apt-get install -y protobuf-compiler python-pil python-lxml python-tk
RUN pip install --no-cache-dir \
        Cython \
        contextlib2 \
        jupyter \
        matplotlib \
        pycocotools \
        opencv-python \
        flask \
        tensorflow \
        Pillow \
        tf_slim \
        requests

# Get protoc 3.0.0, rather than the old version already in the container
RUN curl -OL "https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip" && \
    unzip protoc-3.0.0-linux-x86_64.zip -d proto3 && \
    mv proto3/bin/* /usr/local/bin && \
    mv proto3/include/* /usr/local/include && \
    rm -rf proto3 protoc-3.0.0-linux-x86_64.zip

RUN mkdir -p /usr/src/app

# Run protoc on the object detection repo
RUN cd models/research && \
    protoc object_detection/protos/*.proto --python_out=.

# Set the PYTHONPATH to finish installing the API
ENV PYTHONPATH=$PYTHONPATH:/models/research/object_detection
ENV PYTHONPATH=$PYTHONPATH:/models/research/slim
ENV PYTHONPATH=$PYTHONPATH:/models/research

# set this as the working directory
WORKDIR /usr/src/app

# download the pretrained model
# change here to download your pretrained model
RUN mkdir models && \
    cd models/ && \
    wget "http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03.tar.gz" -O model.tar.gz && \
    tar xzf model.tar.gz && \
    rm model.tar.gz

# copy source code
COPY . .

CMD ["python", "main.py"]
EXPOSE 5001
