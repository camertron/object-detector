from flask import Flask, request, Response
import numpy as np
import cv2
import json
import argparse
import base64

from utilities import load_model, load_label_map, show_inference, parse_output_dict
from custom_np_encoder import NumpyArrayEncoder

model_path = "models/ssd_mobilenet_v1_fpn_shared_box_predictor_640x640_coco14_sync_2018_07_03/saved_model"
labels_path = "data/mscoco_label_map.pbtxt"

vis_threshold = 0.5
max_boxes = 20

detection_model = load_model(model_path)
category_index = load_label_map(labels_path)

# Initialize the Flask application
app = Flask(__name__)

# route http posts to this method
@app.route('/object_detection', methods=['POST'])
def infer():
    # convert image data to uint8
    import pdb
    pdb.set_trace()
    raw_img = base64.b64decode(request.json.get("img"))
    nparr = np.frombuffer(raw_img, np.uint8)

    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    # do inderence on the image and get detections and image data with overlay
    output_dict, image_np = show_inference(detection_model, img, category_index, vis_threshold, max_boxes)
    parsed_output_dict = parse_output_dict(output_dict, category_index)

    _, annotated_img = cv2.imencode(".jpg", image_np)
    parsed_output_dict.update({"img": base64.b64encode(annotated_img).decode("ascii")})

    # add the size of the image in the response
    parsed_output_dict.update({"size": "size={}x{}".format(img.shape[1], img.shape[0])})

    return Response(response=json.dumps(parsed_output_dict), status=200, mimetype="application/json")
  

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)